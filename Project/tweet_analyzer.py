import csv
import json
import spacy
from spacy.lang.en import English
import en_core_web_sm
nlp = en_core_web_sm.load()
from sklearn import preprocessing
le = preprocessing.LabelEncoder()
from sklearn.model_selection import KFold
import nltk
import nltk.tokenize as nt

from difflib import SequenceMatcher

from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
import pickle

import os
import logging
import logging.handlers

import credential_twitter as cred
import crawler_twitter as ct
class TweetAnalyzer:

    def __init__(self, log_dir, debug=False):
        if not log_dir:
            raise ValueError("Missing logging directory argument")
        self.setUpLogging(log_dir, debug)
        self.supported_events = ['earthquake', 'flood', 'fire']
        self.supported_models = ['gaussian', 'random_forest']

    def setUpLogging(self, log_dir, debug):
        log_file = os.path.join(log_dir, 'tweet_analyzer_file.txt')
        level = logging.INFO if debug else logging.ERROR
        handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=1000000, backupCount=5, mode='a', encoding='utf8')
        handler.setLevel(level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(level)
        self.logger.addHandler(handler)

    def is_Relevant(self, json_tweet, query, lang, models):
        if query.lower() not in self.supported_events:
            self.logger.error("Wrong type of disaster entered")
        for model in models:
            if model.lower() not in self.supported_models:
                self.logger.error("Wrong type of model entered")
        tweet_text = json_tweet['text']
        tweet_links = json_tweet['link']
        tweet_hashtags = self.find_Hashtags(tweet_text)
        gpe = self.gpe_Selection(tweet_text)
        pred = self.detect_Relevance(query, tweet_text, tweet_hashtags, tweet_links, gpe, lang, models)
        #cosa faccio con le previsioni???
    
    def detect_Relevance(self, query, text_post, hashtag_post, link_post, gpe_post, lang, models):
        # load the model from disk -- only English!
        gaussian_model = pickle.load(open('gaussian_model_'+str(lang)+'_'+str(query)+'.sav', 'rb'))
        random_forest_model = pickle.load(open('random_forest_model_'+str(lang)+'_'+str(query)+'.sav', 'rb'))
        keywords = []
        with open('Language_word/Multi_Words.json') as words_file:
            data = json.load(words_file)
            for d in data[str(lang.lower())]:
                for p in d[str(query.lower())]:
                    keywords.append(p)

        self.logger.info("Text: "+str(text_post))
        self.logger.info("Hashtag: "+str(hashtag_post))
        self.logger.info("link: "+str(link_post))
        self.logger.info("gpe: "+str(gpe_post))

        if hashtag_post == []:
            t_hashtag_post = 0
        else:
            t_hashtag_post = 99999

        if gpe_post == []:
            t_gpe_post = 0
        else:
            t_gpe_post = 1

        if link_post == '':
            t_link_post = 0
        else:
            t_link_post = 1

        text_list = self.transformRowInFeatures(self.find_presence(text_post, keywords))

        new_X = (t_hashtag_post,t_gpe_post,t_link_post)
        real_new_X =new_X + tuple(text_list)

        prediction = []

        for model in models:
            if model == 'gaussian':
                gp = gaussian_model.predict([real_new_X])
                prediction.append(gp[0])
            if model == 'random_forest':
                rfp = random_forest_model.predict([real_new_X])
                prediction.append(rfp[0])
        return prediction

    def transformRowInFeatures(self,vect):
        return(le.fit_transform(vect))

    def find_presence(self,text,words):
        all_word = self.tokenizationText(text)
        lista = []
        
        flag = 0
        for v in all_word:
            i=0
            while i <len(words):
                val = SequenceMatcher(None, v.lower(), words[i]).ratio()
                if val >= 0.85 :
                    if  flag ==0 :
                        lista.append("1")
                    else:
                        lista[i] = "1"
                else:
                    if flag == 0 :
                        lista.append("0")
                i+=1
            flag = 1

        return lista

    def tokenizationText(self,text):
        all_word = []
        doc = nt.sent_tokenize(text)
        tokenized_sent = [nt.word_tokenize(sent) for sent in doc]
        pos_sentences = [nltk.pos_tag(sent) for sent in tokenized_sent]
        for pos in pos_sentences:
            for p in pos:
                all_word.append(p[0])
        return all_word

    def gpe_Selection(self,d):
        introduction_doc = nlp(d)
        gpe = []
        for token in introduction_doc.ents :
            if token.label_ == 'GPE' :
                gpe.append(token.text)
        return(gpe)

    def find_Hashtags(self, tweet_text):
        hashtags = []
        if '#' in tweet_text:
            word = ''
            j=0
            for char in tweet_text:
                if char in [' ','.'] and j == 1:
                    j=0
                    hashtags.append(word)
                    word=''
                if j == 1:
                    if char != '#':
                        word = word + char
                    else:
                        hashtags.append(word)
                        word=''
                        j=0
                if char == '#':
                    j=1
        return hashtags