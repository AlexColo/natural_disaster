import tweepy
import csv
import json
import string
import nltk
import spacy
import os
import logging
import logging.handlers
from spacy.lang.en import English
import en_core_web_sm
nlp = en_core_web_sm.load()

import nltk.tokenize as nt

from difflib import SequenceMatcher

import credential_twitter as cred
import crawler_twitter as ct
import Translation as tt

class DatasetGenerator:

    def __init__(self, log_dir, debug=False):
        if not log_dir:
            raise ValueError("Missing logging directory argument")
        self.setUpLogging(log_dir, debug)
        self.supported_events = ['earthquake', 'flood', 'fire']
        self.supported_mod = [0,1]

    def setUpLogging(self, log_dir, debug):
        log_file = os.path.join(log_dir, 'dataset_generator_file.txt')
        level = logging.INFO if debug else logging.ERROR
        handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=1000000, backupCount=5, mode='a', encoding='utf8')
        handler.setLevel(level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(level)
        self.logger.addHandler(handler)

    def create_dataset(self,file_name, query, lang, wordnet_lang, yandex_key, mod):
        if query.lower() not in self.supported_events:
            self.logger.error("Wrong type of disaster entered")
        if mod not in self.supported_mod:
            self.logger.error("Wrong mod value: the only possible choice are 0 or 1")
        keywords = []
        flag_lang = 0
        with open('Language_word/Multi_Words.json') as words_file:
            data = json.load(words_file)
            if lang in data:
                flag_lang = 1
                for d in data[str(lang.lower())]:
                    for p in d[str(query.lower())]:
                        keywords.append(p)
        if flag_lang == 0:
            self.logger.info("creation language file")
            file_eng_earthquake = 'original_datasets/dataset_Disaster_Earthquake.csv'
            file_eng_flood = 'original_datasets/dataset_Disaster_Flood.csv'
            file_eng_fire = 'original_datasets/dataset_Disaster_Fire.csv'

            file_name_tran, keywords, tot_tran_keywords = self.generateDataset(file_eng_earthquake, "earthquake", lang, wordnet_lang, yandex_key,0,'')
            keywords_earthquake= self.selectKeyword(file_eng_earthquake, file_name_tran, query, keywords, tot_tran_keywords, lang, wordnet_lang, yandex_key)
            file_name_tran, keywords, tot_tran_keywords = self.generateDataset(file_eng_flood, "flood", lang, wordnet_lang, yandex_key,0,'')
            keywords_flood= self.selectKeyword(file_eng_flood, file_name_tran, query, keywords, tot_tran_keywords, lang, wordnet_lang, yandex_key)
            file_name_tran, keywords, tot_tran_keywords = self.generateDataset(file_eng_fire, "fire", lang, wordnet_lang, yandex_key,0,'')
            keywords_fire= self.selectKeyword(file_eng_fire, file_name_tran, query, keywords, tot_tran_keywords, lang, wordnet_lang, yandex_key)

            with open('Language_word/Multi_Words.json') as words_file:
                data = json.load(words_file)
            data[str(lang).lower()] = []
            p = data[str(lang.lower())]
            keywords_list = {
                            str("earthquake"): keywords_earthquake ,
                            str("flood"): keywords_flood ,
                            str("fire"): keywords_fire
                            }
            p.append(keywords_list)
            with open('Language_word/Multi_Words.json', 'w') as outfile:
                json.dump(data, outfile, indent = 0)

            if query.lower() == "earthquake":
                ALL = keywords_earthquake
                file_eng = file_eng_earthquake
            if query.lower() == "flood":
                ALL = keywords_flood
                file_eng = file_eng_flood
            if query.lower() == "fire":
                ALL = keywords_fire
                file_eng = file_eng_fire

            if mod == 1:
                self.generateDataset(file_eng, query, lang, wordnet_lang, yandex_key,1,ALL)
                exit()
            query = tt.translationYandex(query, lang, yandex_key)
        else:
            ALL = keywords

        #Open/Create a file to append data
        csvFile = open(file_name, 'w', encoding='utf-8', newline='')
        #Use csv Writer
        csvWriter = csv.writer(csvFile, delimiter=';')
        level_val = 0.85

        start_list = ['TEXT','HASHTAG','LINK','GPE']
        for a in ALL :
            start_list.append(a)
        start_list.append('MEDIAS_URL')

        csvWriter.writerow(start_list)

        comp_words = []
        pos_comp_words = []
        for i in range(0,len(ALL)):
            if self.findCompose(ALL[i]):
                comp_words.append(ALL[i])
                pos_comp_words.append(i)
        med_ur = []    
        count = 0

        tweets = ct.TwitterCrawler(cred.CONSUMER_KEY,cred.CONSUMER_SECRET,cred.ACCESS_TOKEN,cred.ACCESS_TOKEN_SECRET,"programmi di test").crawl(query, lang)

        for tweet in tweets:
            try :
                medias = tweet.extended_entities['media']
                #MEDIAS_URL
                for med in medias:
                    m = med["url"]          
                if m in med_ur:
                    next
                else:
                    med_ur.append(m)
                    #HASHTAGS
                    hashtags = tweet.entities["hashtags"]
                    h = []
                    for hashtag in hashtags:
                        h.append(hashtag["text"])
                    #LINKS
                    urls = tweet.entities["urls"]
                    u = ""
                    for url in urls:
                        u = url["url"]                
                    #GPE
                    gpe = self.GPE_Selection(tweet.full_text)
                    #COMMON WORD
                    vect_common = self.find_Common_Word(tweet.full_text,ALL,level_val)
                    if len(comp_words)>0:
                        vect_common = self.compose_Word(tweet.full_text,pos_comp_words,comp_words,vect_common)
            
                    all_together = [tweet.full_text, h, u, gpe]
                    for v in vect_common:
                        all_together.append(v)
                    all_together.append(m)
            
                    csvWriter.writerow(all_together)
                    count +=1
        
            except :
                next

    def tokenizationText(self,text):
        all_word = []
        doc = nt.sent_tokenize(text)
        tokenized_sent = [nt.word_tokenize(sent) for sent in doc]
        pos_sentences = [nltk.pos_tag(sent) for sent in tokenized_sent]
        for pos in pos_sentences:
            for p in pos:
                all_word.append(p[0])
        return all_word

    def find_Common_Word(self,text,keywords,level_val) :
        all_word = self.tokenizationText(text)
        lista = []
        flag = 0
        for v in all_word:
            i=0
            while i < len(keywords):
                val = SequenceMatcher(None, v.lower(), keywords[i]).ratio()
                if val >= level_val :
                    if flag == 0 :
                        lista.append("1")
                    else:
                        lista[i] = "1"
                else:
                    if flag ==0:
                        lista.append("0")
                i+=1
            flag = 1
        return lista

    def compose_Word(self, texts, pos, words,lista):
        i=0
        while i < len(words):
            if words[i] in texts.lower():
                p=pos[i]
                lista[p] = "1"
            i+=1
        return lista

    def GPE_Selection(self,d):
        introduction_doc = nlp(d)
        gpe = []
        for token in introduction_doc.ents :
            if token.label_ == 'GPE' :
                gpe.append(token.text)
        return(gpe)

    def findCompose(self,word):
        all_word = self.tokenizationText(word)
        if len(all_word) > 1:
            return True
        else:
            return False

    def controlComposte(self,texts, pos, words, lista):
        val = 0
        i=0
        while i < len(words):
            if words[i] in texts.lower():
                p=pos[i]
                if lista[p] == "0":
                    lista[p] = "1"
                    val += 1
            i+=1
        return (val,lista)

    def find_presence(self,text,words):
        tot = 0
        all_word = self.tokenizationText(text)
        lista = []
        
        flag = 0
        for v in all_word:
            i=0
            while i <len(words):
                val = SequenceMatcher(None, v.lower(), words[i]).ratio()
                if val >= 0.85 :
                    if  flag ==0 :
                        lista.append("1")
                    else:
                        lista[i] = "1"
                else:
                    if flag == 0 :
                        lista.append("0")
                i+=1
            flag = 1
        for val in lista:
            tot = tot + int(val)
        return(tot, lista)

    def generateDataset(self, file_name, query, lang, wordnet_lang,yandex_key, flag,tran_keywords):
        with open(file_name, encoding="utf8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            keywords = []
            with open('Language_word/Multi_Words.json') as words_file:
                data = json.load(words_file)
                for d in data["en"]:
                    for p in d[str(query.lower())]:
                        keywords.append(p)
            len_words = len(keywords)+1

            texts = []
            words = []
            comp_words = []
            pos_comp_word = []

            count_line = 0
            tot_summon = 0

            text = []
            hashtag = []
            link = []
            gpe = []
            relevance = []
            syn = []
            
            for row in csv_reader:
                if row and count_line != 0 :
                    text.append(row[0])
                    hashtag.append(row[1])
                    link.append(row[2])
                    gpe.append(row[3])
                    for i in range(1,len_words):
                        tot_summon = tot_summon +int(row[i+3])
                    relevance.append(row[i+5])
                count_line += 1

            # Open/Create a file to append data
            if flag == 0:
                csvFile = open('support_datasets/dataset_Disaster_'+str(query)+'_'+str(lang)+'_SUPPORT.csv', 'w', encoding='utf-8', newline='')
            if flag == 1:
                csvFile = open('final_translated_datasets/dataset_Disaster_'+str(query)+'_'+str(lang)+'.csv', 'w', encoding='utf-8', newline='')
            #Use csv Writer
            csvWriter = csv.writer(csvFile, delimiter=';')
            start_list = ['TEXT','HASHTAGS','LINKS','GPE']

            pos = 0

            if flag == 0:
                for x in keywords:
                    wor = tt.translationYandex(x,lang,yandex_key)
                    if wor not in words:
                        words.append(wor)
                        if self.findCompose(wor):
                            comp_words.append(wor)
                            pos_comp_word.append(pos)
                        start_list.append(wor)
                        pos+=1
                        if wordnet_lang != 'None':
                            syn = tt.findSynonim(wor,wordnet_lang)
                        if syn != []:
                            for s in syn:
                                if s not in words :
                                    words.append(s)
                                    if self.findCompose(s):
                                        comp_words.append(s)
                                        pos_comp_word.append(pos)
                                    start_list.append(s)
                                    pos+=1
            if flag == 1:
                for x in tran_keywords:
                    words.append(x)
                    start_list.append(x)
                    if self.findCompose(x):
                        comp_words.append(x)
                        pos_comp_word.append(x)
                    pos+=1

            start_list.append('')
            start_list.append('Y')

            csvWriter.writerow(start_list)
            
            count = 2
            self.logger.info("analysis in progess")
            for x in text:
                t = tt.translationYandex(x,lang,yandex_key)
                texts.append(t)
                count+=1

            for i in range(0, len(texts)):
                ident = 0
                all_together = [texts[i],hashtag[i],link[i],gpe[i]]
                val, lista = self.find_presence(x,words)
                while ident < len(pos_comp_word):
                    val, lista = self.controlComposte(x,pos_comp_word,comp_words,lista)
                    ident+=1
                for l in lista:
                    all_together.append(l)
                all_together.append('')
                all_together.append(relevance[i])
                csvWriter.writerow(all_together)
            if flag == 0:
                return('support_datasets/dataset_Disaster_'+str(query)+'_'+str(lang)+'_SUPPORT.csv', keywords, words)

    def find_Syn(self,word,wordnet_lang):
        syn = [word]
        synonim = []
        if wordnet_lang != 'None':
            synonim = tt.findSynonim(word,wordnet_lang)
        for s in synonim:
            if s not in syn:
                syn.append(s)
        return len(syn), syn


    def selectKeyword(self,file_name_eng, file_name_tran,query,keywords,tot_tran_keywords,lang,wordnet_lang,yandex_key):
        lista_eng = []
        lista_tran = []
        indice = []
        d_tran={}
        d_eng={}
        d_fin=[]
        tran_word=[]
        perc_val=[]
        tot_tran_word = []
        val_vect= []
        indice_tran = 1
        indice_eng = 1  
        ele = 0
        with open(file_name_tran, encoding="utf8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            len_words=len(tot_tran_keywords)+1
            count_line=0
            for i in range(1,len_words):
                d_tran['v'+str(i)]=[]

            for k,v in d_tran.items():
                exec('{0}={1}'.format(k,v))

            for row in csv_reader:
                if row :
                    for i in range(1,len_words):
                        d_tran['v'+str(i)].append(row[i+3])

                count_line += 1

        with open(file_name_eng, encoding="utf8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            len_words=len(keywords)+1
            count_line=0

            for i in range(1,len_words):
                d_eng['v'+str(i)]=[]

            for k,v in d_eng.items():
                exec('{0}={1}'.format(k,v))

            for row in csv_reader:
                if row :
                    for i in range(1,len_words):
                        d_eng['v'+str(i)].append(row[i+3])

                count_line += 1

        syn_vect = []
        while indice_eng < len(keywords)+1:
            val_eng = 0

            while tt.translationYandex(d_eng['v'+str(indice_eng)][0], lang, yandex_key) in syn_vect :
                indice_eng +=1

            n_syn, syn_val = self.find_Syn(d_tran['v'+str(indice_tran)][0],wordnet_lang)
            for s in syn_val:
                syn_vect.append(s)
            n_syn = n_syn + indice_tran
            while indice_tran < n_syn:
                val_eng = 0
                tot_tran = 0
                val_tran = 0
                i=0
                while i in range(0, len(d_eng['v'+str(1)])): 
                    if d_eng['v'+str(indice_eng)][i] == '1':
                        val_eng += 1
                        if d_tran['v'+str(indice_tran)][i] == '1':
                            val_tran +=1
                    if d_tran['v'+str(indice_tran)][i] == '1':
                            tot_tran +=1
                    i+=1
                if val_eng != 0:
                    po = (d_tran['v'+str(indice_tran)][0], ((val_tran*tot_tran)/(val_eng*val_eng)))   
                    d_fin.append(po)
                    if val_tran != 0:
                        q=val_tran/val_eng
                        perc_val.append(q)
                else:
                    ele+=1
                    indice.append(indice_tran)
                indice_tran+=1
            indice_eng+=1
        fin_ord = sorted(d_fin, key = lambda x: float(x[1]), reverse = True)
        n_key = len(keywords)
        if wordnet_lang == 'None':
            n_key = len(tot_tran_keywords)
        i = 0
        while i < (n_key-ele):
            c = fin_ord[i]
            tran_word.append(c[0])
            i+=1
        j = i
        for i in indice:
            if j < n_key:
                tran_word.append(d_tran['v'+str(i)][0])
            j+=1
        return(tran_word)