import csv
import json
import os
import logging
import logging.handlers
import spacy
from spacy.lang.en import English
import en_core_web_sm
nlp = en_core_web_sm.load()

from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier
from sklearn.datasets import make_classification
import pickle
import time
from sklearn import preprocessing
#creating labelEncoder
le = preprocessing.LabelEncoder()
# Converting string labels into numbers.

class ModelFitter:

    def __init__(self,log_dir, debug=False):
        if not log_dir:
            raise ValueError("Missing logging directory argument")        
        self.setUpLogging(log_dir, debug)
        self.supported_events = ['earthquake', 'flood', 'fire']

    def setUpLogging(self, log_dir, debug):
        log_file = os.path.join(log_dir, 'model_fitter_file.txt')
        level = logging.INFO if debug else logging.ERROR
        handler = logging.handlers.RotatingFileHandler(log_file, maxBytes=1000000, backupCount=5, mode='a', encoding='utf8')
        handler.setLevel(level)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        handler.setFormatter(formatter)
        self.logger = logging.getLogger(__name__)
        self.logger.setLevel(level)
        self.logger.addHandler(handler)

    def learn_new_model(self,file_name, query, lang):
        if query.lower() not in self.supported_events:
            self.logger.error("Wrong type of disaster entered")
        with open(file_name, encoding="utf8") as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=';')
            gpe_tweet = []
            link_tweet = []
            hashtag_tweet = []
            keywords = []

            with open('Language_word/Multi_Words.json') as words_file:
                data = json.load(words_file)
                for d in data[str(lang.lower())]:
                    for p in d[str(query.lower())]:
                        keywords.append(p)

            len_words = len(keywords)+1
            label = []

            start_time = time.perf_counter()

            d={}
            for i in range(1,len_words):
                d['v'+str(i)]=[]

            for k,v in d.items():
                exec('{0}={1}'.format(k,v))

            count_line = 0

            prova_h = []

            for row in csv_reader:
                if row and count_line != 0 :
                    if row[3] != '[]':
                        gpe_tweet.append("yes")
                    else:
                        gpe_tweet.append("no")

                    if row[2] != '':
                        link_tweet.append("yes")
                    else:
                        link_tweet.append("no")

                    prova_h.append(row[1])
                    for i in range(1,len_words):
                        d['v'+str(i)].append(row[i+3])
                                        
                    label.append(row[len_words+4])
                    
                count_line += 1

            n=count_line
            label=self.transformRowInFeatures(label)
            conta1 = 0
            conta2 = 0
            for la in label:
                if la>0:
                    conta1+=1
                if la>1:
                    conta2+=1
            test_gpe_feature = []
            test_link_feature = []
            test_h_feature = []

            gpe_feature = self.transformRowInFeatures(gpe_tweet)
            link_feature = self.transformRowInFeatures(link_tweet)
            prova_feature_hash = self.transformRowInFeatures(prova_h)
            
            for i in range(1,len_words):
                d['v'+str(i)] = self.transformRowInFeatures(d['v'+str(i)])

            dic={}
            for i in range(0,n-1):
                dic['f'+str(i)]=[]

            for i in range (0,n-1):
                for k,v in d.items():
                    dic['f'+str(i)].append(v[i])

            prova_features = zip(prova_feature_hash,gpe_feature,link_feature,[dic['f'+str(i)] for i in range(0,n-1)])
            prova_features = list(prova_features)
            real_features = self.createListFeatures(prova_features)

            self.trainModel(real_features,label,start_time,len_words,n,query,lang)

    def fScore(self,recall, precision):
        fscore = (2*(precision*recall))/(precision+recall)
        self.logger.info("f-score "+str(fscore))

    def valRecall(self,y_test,y_pred,val):
        c=0
        count_test = 0
        true_pos = 0
        false_pos = 0
        false_neg = 0
        self.logger.info("value > "+str(val))
        for i in y_test:
            if int(i) > val:
                count_test +=1
                if int(y_pred[c]) > val:
                    true_pos +=1
                else:
                    false_neg +=1
            else:
                if int(y_pred[c]) > val :
                    false_pos +=1
            c +=1
        tot_pos_pred = 0
        for pos in y_pred:
            if pos>val:
                tot_pos_pred+=1
        precision = true_pos/(true_pos+false_pos)
        recall=true_pos/(true_pos+false_neg)
        self.logger.info("precision "+str(precision))
        self.logger.info("recall "+str(recall))
        self.fScore(recall,precision)
        filtering=1-(tot_pos_pred/c)
        self.logger.info("filter "+str(filtering)+"\n")
        
    def gaussianMethod(self,X_train,y_train,X_test,y_test,start_time,n,query,lang):
        #Create a Gaussian Classifier
        gnb = GaussianNB()
        #Train the model using the training sets
        gnb.fit(X_train, y_train)
        # save the model to disk
        filename = 'gaussian_model_'+str(lang)+'_'+str(query)+'.sav'
        pickle.dump(gnb, open(filename, 'wb'))        
        #Predict the response for test dataset
        y_pred = gnb.predict(X_test)
        now = time.perf_counter()
        end_time = now - start_time
        self.logger.info("total time "+ ':{:0.6f}'.format(end_time)+" s")
        self.logger.info("time for tweet "+ ':{:0.6f}'.format( (end_time/(n-1))*1000)+" ms\n")

        self.valRecall(y_test,y_pred,0)
        self.valRecall(y_test,y_pred,1)

    def forestMethod(self,X_train,y_train,X_test,y_test,interval_time,start_time,len_words,n,query,lang):
        self.logger.info("\nRANDOM FOREST:")
        #create a Random Forest classifier
        clf = RandomForestClassifier(n_estimators=n,max_depth=(len_words+3))
        #train the model
        clf.fit(X_train, y_train)
        # save the model to disk
        filename = 'random_forest_model_'+str(lang)+'_'+str(query)+'.sav'
        pickle.dump(clf, open(filename, 'wb'))
        #Predict the response for test dataset
        y_pred = clf.predict(X_test)

        now = time.perf_counter()
        end_time = now - (start_time + interval_time)
        self.logger.info("total time "+ ':{:0.6f}'.format(end_time)+" s")
        self.logger.info("time for tweet "+ ':{:0.6f}'.format((end_time/(n-1))*1000)+" ms\n")


        self.valRecall(y_test,y_pred,0)
        self.valRecall(y_test,y_pred,1)

    def trainModel(self,prova_features,label,start_time,len_words,n,query,lang):
        from sklearn.model_selection import train_test_split
        # Split dataset into training set and test set
        X_train, X_test, y_train, y_test = train_test_split(prova_features, label, test_size=0.3)

        conta1 = 0
        conta2 = 0
        for la in y_test:
            if la>0:
                conta1+=1
            if la>1:
                conta2+=1
        self.logger.info("test 1+2 "+str(1-(conta1/len(y_test))))
        self.logger.info("test 2 "+str(1-(conta2/len(y_test)))+"\n")

        now = time.perf_counter()
        partial_time1 = now - start_time
        self.gaussianMethod(X_train,y_train,X_test,y_test,start_time,n,query,lang)
        now = time.perf_counter()
        partial_time2 = now - start_time
        interval_time = partial_time2 - partial_time1
        self.forestMethod(X_train,y_train,X_test,y_test,interval_time,start_time,len_words,n,query,lang) 


    def transformRowInFeatures(self,vect):
        return(le.fit_transform(vect))

    def extractionValue(self,vect):
        for val in vect:
            yield(val)

    def createListFeatures(self,features):
        real=()
        for pf in features:
            c=self.extractionValue(pf[3])
            a = (pf[0],pf[1],pf[2])
            for i in c:
                a = a + (i,)
            real = real + (a,)
        return(list(real))