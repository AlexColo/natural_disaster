import nltk
from nltk.corpus import wordnet

import yandex
from yandex_translate import YandexTranslate
'''
languages = ['it','es','fr','de','he','bg','hu','sr','lt','el','et','tr','ru','cs','ar','ko','th','fa',
            'ca','in','ro','tl','fi','da','nl','zh','pt','sk','ml','sl','hi','ja','hr','uk','lt','no',
            'sv','pl','vi'] 
            #NON HO MESSO en PERCHE' QUELLO DI BASE E' IN INGLESE
'''
#QUELLI CON 0 FINALE NON HANNO IL CODICE IDENTIFICATIVO IN WORDNET (non li gestisco?)
'''
wordnet_lang = ['ita','spa','fra','ger0','heb','bul','hun0','ser0','lat0','ell','est0','tur0','rus0','ceco','arb','kor0','tha','fas',
                'cat','ind','ron','tal0','fin','dan','nld','cmn','por','slk','mal0','slv','hin0','jpn','hrv','ukr0','lit','nno',
                'swe','pol','viet0'
'''
def findSynonim(word,lang):
    synonim = []
    for syn in wordnet.synsets(word):
        for lemma in syn.lemmas(lang=lang):
            synonim.append(lemma.name())
    return(synonim)

def translationYandex(text,lang,yandex_key):
    translate = YandexTranslate(yandex_key)
    translations = translate.translate(text, 'en-'+str(lang))
    for x in translations['text']:
        return(x)
